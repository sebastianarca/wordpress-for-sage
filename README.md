# Para completar la instalacion:
Despues de haber ejecutar "docker-compose up", es necesario seguir algunos pasos.

**/opt/install.sh** contiene todas las instrucciones para instalar wordpress y sage



Para entrar al contenedor ejecutar:

**docker exec -ti wordpress bash**

ir al directorio del plugin con:

**cd wp-content/themes/new_theme/**

Se recomienda usar en VSCode el Plugin "Dev Container"

https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers



Para quitar Tailwind y usar Bootstrap, es necesario seguir las instrucciones de la documentacion:

https://roots.io/sage/docs/replacing-tailwind-with-bootstrap/

# Wordpress For Sage
Este repositorio, contiene un archivo Dockerfile y un Docker Compose, para ambiente de Desarrollo local.

El Dockerfile parte de la imagen **wordpress:6.1.1-php8.0-apache** y se instalan otras herramientas necesarias para el desarrollo tales como:
 - WP Cli. Disponible bajo el comando **wp**
 - Yarn
 - NodeJS
 - Composer


Una vez se ejecute el contenedor y uno trate de entrar por bash, estara bajo el nombre de usuario **www-data** que es el definido por la imagen de Wordpress durante la instalacion.

Si el directorio **www** esta vacio, Wordpress sera descargado, instalado y configurado desde Cero.


# PHP My Admin

Con el motivo de facilitar el acceso a la DB por parte de dev menos experimentades, se facilita la instalacion de PHPMyAdmin, el cual es accesible de esta forma http://localhost:8080/.


Si se utilizan los valores tal cual esta en la configuracion de docker-compose.yml, los datos de conexion  son:
 - Servidor: db
 - Usuario: root
 - Contraseña: 1234
 
# Compilacion y ejecucion inicial

Dentro del directorio, para compilar la instalacion se debe usar el comando `docker-compose build` y luego para iniciar se puede usar `docker-compose up`.

Al ejecutar este ultimo comando la consola quedara bloqueada mostrando el Log de las cosas que sucedan, al salir de la consola el servidor sera detenido, para 
evitar que esto suceda se puede ejecutar el servidor Docker con la opcion detachado, que lo dejara en segundo plano. `docker-compose up -d`.

Posteriormente para apagar el servidor se puede usar `docker-compose down`.
