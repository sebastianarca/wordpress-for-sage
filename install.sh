#!/bin/bash
/usr/local/bin/wp core install --path='/var/www/html' --url=localhost --title='Mi compu tiene un Wordpres fantastico' --admin_user='admin' --admin_password='admin' --locale=ar_AR --admin_email='sebastian@navebinario.com' --skip-email || exit 1;

wp language core install es_AR || exit 1;
wp language core activate es_AR || exit 1;

cd /var/www/html/wp-content;

# check if directory wp-content/plugins/acorn/ exists
if [ ! -d "/var/www/html/wp-content/plugins/acorn" ]; then
    wp plugin install https://github.com/roots/acorn/releases/download/v2.1.2/acorn-v2.1.2-php-8.0.zip;
fi
wp plugin activate acorn;

if [ ! -d "/var/www/html/wp-content/themes/new_theme" ]; then
    cd /var/www/html/wp-content/themes/
    composer create-project roots/sage new_theme
    
    cd /var/www/html/wp-content/themes/new_theme
    yarn install
fi
wp theme activate new_theme || exit 1;


cd /var/www/html/wp-content/themes/new_theme
composer install
yarn install
yarn build

