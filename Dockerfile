# https://github.com/docker-library/wordpress/blob/master/latest/php8.0/apache/Dockerfile
# https://github.com/docker-library/wordpress/blob/master/latest/php8.0/apache/docker-entrypoint.sh
FROM wordpress:6.1.1-php8.0-apache AS build

RUN cd /opt; \
    curl https://nodejs.org/dist/v18.13.0/node-v18.13.0-linux-x64.tar.xz --output node.tar.xz; \
    tar -xvf node.tar.xz; \
    ln -s /opt/node-v18.13.0-linux-x64/bin/node /usr/local/bin/node; \
    ln -s /opt/node-v18.13.0-linux-x64/bin/npm /usr/local/bin/npm; \
    ln -s /opt/node-v18.13.0-linux-x64/bin/npx /usr/local/bin/npx; \
    ln -s /opt/node-v18.13.0-linux-x64/bin/corepack /usr/local/bin/corepack; \
    rm node.tar.xz; \
    \
    \
    corepack enable; \
    corepack prepare yarn@stable --activate; \
    \
    \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer; \
    \
    \
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar; \
    chmod +x wp-cli.phar; \
    mv wp-cli.phar /usr/local/bin/wp; \
    \
    apt-get update; \
    apt-get install git unzip -y; \
    \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*;

RUN mkdir /var/www/.cache && chmod 777 /var/www/.cache; \
    mkdir /var/www/.yarn && chmod 777 /var/www/.yarn 

RUN chown -R www-data:www-data /var/www
USER www-data
WORKDIR /var/www/html

RUN rm -rf /opt/install.sh
COPY install.sh /opt/install.sh

# ENTRYPOINT ["/opt/install.sh"]

EXPOSE 80